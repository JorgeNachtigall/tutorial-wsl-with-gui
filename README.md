# Tutorial de configuração do Windows Subsystem for Linux + GUI via Xming - Somente Windows 10

Neste tutorial vamos configurar o WSL e como vincular o Xming para que possamos ter uma interface gráfica para executar aplicações e um gerenciador de arquivos.

## Habilitando o WSL

- Abra o PowerShell como administrador e execute o seguinte comando:

> Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux

- Reinicie seu computador.


## Instalando uma distribuição Linux

- Utilizando a barra de pesquisa do Windows, procure pelo aplicativo Microsoft Store e abra-o (talvez seja necessário criar uma conta).
- Dentro do aplicativo da Microsoft Store pesquise por "linux".
- Aparecerão algumas distribuições linux em forma de aplicativo para você escolher. Neste tutorial iremos usar o Ubuntu.
- Para instalar basta clicar na distro e clicar em INSTALAR.

## Execução inicial do WSL

- Uma vez instalado, agora você pode pesquisar em seu computador por "Ubuntu" (ou o nome da distribuição que você instalou) e um aplicativo abrirá um console de comando.
- Na primeira vez que você iniciar a aplicação será requisitado um nome para o usuário root e uma senha.
- Após isso você já estará rodando uma distribuição Linux dentro do seu Windows 10.

# *(Opcional)* Instalando o Z Shell (zsh)

- Com o console rodando sua distribuição Linux execute:

> sudo apt-get

> sudo apt-get upgrade

> sudo apt-get install zsh

- Abre novamente o terminal de execução da sua distribuição Linux.
- Escolha a opção de configuração inicial.
- O Z Shell está instalado.

# Instalação e integração do Xming (X server para rodar aplicações gráficas)

## Instalação do Xming

- Existem diversos aplicativos X server, neste tutorial iremos o Xming.
- No seu Windows 10 faça download do Xming e instale-o: ![Download](https://sourceforge.net/projects/xming/).
- A instalação do Xming pode ser feita com todas as opções default, não sendo necessário modificar nada.

## Integrando Xming a sua distro Linux

- Abra o arquivo de configuração do seu console, no meu caso como estou usando ZShell o arquivo pode ser aberto com o editor de texto VIM utilizando o seguinte comando:

> sudo vim ~/.zshrc

Se você está usando Bash:

> sudo vim ~/.bashrc

- Adiciona a seguinte linha ao arquivo .zshrc:

> export DISPLAY=:0.0

- Feche e salve o arquivo.

# *(Opcional)* Instalando gerenciados de arquivos Nautilus:

- Execute no console da sua distribuição Linux:

> sudo apt-get install nautilus

# *(Opcional)* Instalação do Sublime Text:

- Execute no console da sua distribuição Linux:

> sudo apt-get update

> sudo apt-get install sublime-text

- O sublime necessita também de alguns componentes gráficos para executar, você pode instalá-los executando:

> sudo apt-get install libgtk2.0-0

- Caso o instalador não crie um link para o executável do sublime, para que seja possível acessá-lo através do comando '''shell subl ''' no seu console, execute o seguinte comando:

> ln -s /opt/sublime_text/sublime_text /usr/local/bin/sublime_text

# Rodando as aplicações gráficas através do Xming

- No Windows 10 execute a aplicação Xming.
- Certifique-se de que ela está rodando checando se o ícone da mesma está aberto ao lado do relógio do Windows. Ao descansar o mouse sobre o ícone deve aparecer a seguinte mensagem:

> Xming Server:0.0


*Importante: Sempre que você for executar um aplicativo com interface gráfica é necessário que o Xming esteja rodando.*

- Para executar o gerenciador de arquivos Nautilus basta escrever no seu console:

> nautilus

- Para executar o Sublime basta escrever no seu console:

> subl


ou caso esteja no diretório do arquivo o qual deseja abrir no sublime:

> subl nome_do_arquivo

# PRONTO :)